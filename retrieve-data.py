import requests

import math
import numpy as np, pandas as pd
from datetime import timedelta
from pandas import datetime
from pandas import DataFrame
import seaborn as sns

import matplotlib.pyplot as plt

DS_USERNAME = "tenant@thingsboard.org"
DS_PASSWORD = "2B8TDrvw"
DS_HOST     = "http://192.168.1.58:8080"
DS_AUTH_URL = DS_HOST + "/api/auth/login"
DS_URL      = DS_HOST + "/api/plugins/telemetry/DEVICE/"

def fetchData(devId, keys, startTs, endTs, limit=100000):
	global DS_USERNAME
	global DS_PASSWORD
	global DS_AUTH_URL
	global DS_URL

	headers = {
        "Content-Type": "application/json;charset=UTF-8"
    	}
	payload = '{"username": "'+DS_USERNAME+'","password": "'+DS_PASSWORD+'"}'
	response = requests.post(DS_AUTH_URL, data=payload, headers=headers)
	print("Response Code:"+str(response.status_code))

	token = "Bearer " + response.json()["token"]
	#Actual API call
	headers = {
        "X-Authorization": token,
		"Content-Type" : "application/json;charset=UTF-8"
    }

	# Get keys
	url = DS_URL + devId + "/keys/timeseries"
	print(url)
	response = requests.get(url, headers=headers)
	print("Available keys: " + response.text)

	url = DS_URL + devId + "/values/timeseries?limit="+str(limit)+"&keys="+(','.join(map(str, keys)))+"&startTs="+ startTs +"&endTs=" + endTs
	print(url)
	# url = DS_HOST + "/api/plugins/telemetry/DEVICE/d3e83ce0-26a3-11ec-b807-bdca9a00d16b/values/timeseries?keys=temperature"
	response = requests.get(url, headers=headers)
	print("content: " + response.text)
	if response.status_code != 200:
		print(response.status_code)
		print(response.text)
		return

	data = pd.DataFrame()

	for key in keys:
		print(key)
		json = response.json()
		if key in json:
			df = pd.json_normalize(json[key])
			df['ts'] = pd.to_datetime(df['ts'], unit="ms") + pd.DateOffset(hours=8)
			# df['ts'] = df['ts'].values.astype('<M8[m]')
			# df.dt.index.to_period("m").to_timestamp()
			df[key] = df['value'].astype(float)
			df.set_index('ts', inplace=True)
			df.index = pd.to_datetime(df.index)
			# df.dt.index.to_period("m").to_timestamp()
			# del df.index.name
			df = df.drop(columns="value")
			# df.info()
			data = pd.concat([data, df], axis=1)

	return data

def mean_std(df, by_col="month"):
    mn = df.groupby([by_col]).mean()
    std = df.groupby([by_col]).std()
    print(std)

    return mn, std

def visualisation(mean, std):
    plt.figure()
    # x = [1,2,3,4,5,6,7,8,9,10,11,12]

    plt.subplot(311)
    lines = plt.plot(mean)
    #lines[2].set_linestyle(':')
    lines[1].set_linestyle('-.')
    lines[0].set_linestyle('-')
    plt.yscale('linear')
    plt.xticks(np.arange(min(mean.index), max(mean.index)+1, 1.0))
    plt.title('Mean of ')
    plt.legend(lines[:3], ['Total Rainfall','Maximum Rainfall in a day','Num of rainy day'],framealpha=1, frameon=True)
    plt.grid(True)

    plt.subplot(312)
    lines = plt.plot(std)
    # lines[2].set_linestyle(':')
    lines[1].set_linestyle('-.')
    lines[0].set_linestyle('-')
    plt.yscale('linear')
    plt.xticks(np.arange(min(std.index), max(std.index)+1, 1.0))
    plt.title('Standard Deviation')
    plt.legend(lines[:3], ['Total Rainfall','Maximum Rainfall in a day','Num of rainy day'],framealpha=1, frameon=True)
    plt.grid(True)

    plt.subplot(313)
    # # lines[2].set_linestyle(':')
    lines[1].set_linestyle('-.')
    lines[0].set_linestyle('-')
    plt.yscale('linear')
    plt.yscale('linear')
    plt.xticks(np.arange(min(std.index), max(std.index)+1, 1.0))
    plt.title('Standard Deviation')
    plt.legend(lines[:3], ['Total Rainfall','Maximum Rainfall in a day','Num of rainy day'],framealpha=1, frameon=True)
    plt.grid(True)

    # Adjust the subplot layout, because the logit one may take more space
    # than usual, due to y-tick labels like "1 - 10^{-3}"
    plt.subplots_adjust(top=0.92, bottom=0.08, left=0.10, right=0.95, hspace=0.25, wspace=0.35)
    plt.show()


def boxChart(df1, df2, key1, key2):
	# Draw Plot
	fig, axes = plt.subplots(1, 2, figsize=(20,7), dpi= 80)
	sns.boxplot(x='bal_hour', y=key1, data=df1, ax=axes[0]).set(xlabel='Hour of the Day', ylabel='Light')
	sns.boxplot(x='cor_hour', y=key2, data=df2).set(xlabel='Hour of the Day', ylabel='Light')
	# sns.boxplot(x='light', y=key3, data=df)

	# Set Title
	axes[0].set_title('Hour-wise Boxplot\n(Balcony)', fontsize=18)
	axes[1].set_title('Hour-wise Boxplot\n(Corridor)', fontsize=18)
	# axes[2].set_title('Hour-wise Box Plot\n(Light)', fontsize=18)
	plt.show()

if __name__ == "__main__":
    PROBE_Corridor = "10b8a040-5cd4-11ec-8619-d3619a138d77"
    PROBE_Balcony = "d3e83ce0-26a3-11ec-b807-bdca9a00d16b"
    keys = ["light"] #["altitude","humidity","light","pressure","temperature","uv","soilmoisture"]
    startDT = datetime(year = 2021, month = 12, day = 15, hour = 0, minute = 0, second = 0) # + timedelta(hour	s=8)
    endDT = datetime(year = 2021, month = 12, day = 31, hour = 23, minute = 59, second = 59) # + timedelta(hours=8)

    try:
        # data = fetchData(PROBE_Balcony, keys, str(int(datetime.timestamp(startDT)))+"000", str(int(datetime.timestamp(endDT)))+"000", limit=100000)
        # print(data)
        # data.to_csv("sample data.csv")

        # data['timestamp'] = [t.strftime('%y-%m-%d %H:%M') for t in data.index]
        # print(data.groupby('timestamp').agg(['count']))
        
        # print(data.max())
        # print(data.min())
        # print(data.mean())
        # print(data.median())
        # print(data.std())
        # print(data.groupby(['distance']).size())

        df_balcony = fetchData(PROBE_Balcony, keys, str(int(datetime.timestamp(startDT)))+"000", str(int(datetime.timestamp(endDT)))+"000", limit=100000)
        df_balcony["bal_hour"] = df_balcony.index.hour
        df_balcony.rename(columns={'light':'bal_light'}, inplace=True)
        print(df_balcony)

        df_corridor = fetchData(PROBE_Corridor, keys, str(int(datetime.timestamp(startDT)))+"000", str(int(datetime.timestamp(endDT)))+"000", limit=100000)
        df_corridor["cor_hour"] = df_corridor.index.hour
        df_corridor.rename(columns={'light':'cor_light'}, inplace=True)
        print(df_corridor)

        # df_moisture = fetchData(PROBE_Balcony, keys, str(int(datetime.timestamp(startDT)))+"000", str(int(datetime.timestamp(endDT)))+"000", limit=100000)
        # df_light = fetchData(PROBE_Balcony, keys, str(int(datetime.timestamp(startDT)))+"000", str(int(datetime.timestamp(endDT)))+"000", limit=100000)

        # df = pd.concat([df_balcony, df_corridor], axis=1)
        # df["bal_hour"] = df["bal_hour"].astype(int)
        # print(df)

        # Refer to this tutorial about the box representation of distribution
        # https://seaborn.pydata.org/tutorial/categorical.html#categorical-tutorial
        # https://muse.union.edu/dvorakt/what-drives-the-length-of-whiskers-in-a-box-plot/ 
        boxChart(df_balcony,df_corridor,'bal_light', 'cor_light')
    except KeyboardInterrupt:
        print('shutting down the program')
	
    