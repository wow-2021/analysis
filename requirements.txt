pandas>=0.25.1
numpy>=1.21.1
matplotlib>=3.4.2
statsmodels>=0.10.0
sklearn>=0.0
seaborn>=0.11.1
keras>=2.3.1
requests>=2.22.0
pmdarima>=1.8.2
scipy>=1.4.1
plotly>=5.1.0
pydlm>=0.1.1.11